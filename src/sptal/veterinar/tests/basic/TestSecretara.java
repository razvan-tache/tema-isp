package sptal.veterinar.tests.basic;

import static org.junit.Assert.*;

import org.junit.Test;

import spital.veterinar.Secretara;

public class TestSecretara {

	@Test
	public void testEsteSecretara() {
		Secretara secretara = new Secretara("Nume secretara", (float)2000);
		
		assertEquals("Secretara", secretara.getPozitie());
	}

}
