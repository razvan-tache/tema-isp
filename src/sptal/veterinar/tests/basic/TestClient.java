package sptal.veterinar.tests.basic;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import spital.veterinar.Animal;
import spital.veterinar.Client;
import spital.veterinar.Secretara;

public class TestClient {
	
	private Client client;

	@Before
	public void init() {
		client = new Client("Razvan Tache", "Splaiul independentei");
	}
	
	@Test
	public void testAddAnimal() {
		Animal animal = new Animal(2, "C�ine", "Cocar");
		
		client.addAnimal(animal);
		
		assertTrue(client.getAnimalList().contains(animal));
	}

	@Test
	public void testAddMoreAnimals() {
		Animal animal = new Animal(2, "C�ine", "Cocar");
		Animal animal2 = new Animal(3, "C�ine", "Cocar");
		
		client.addAnimal(animal);
		client.addAnimal(animal2);
		assertEquals(2, client.getAnimalList().size());
	}
}
