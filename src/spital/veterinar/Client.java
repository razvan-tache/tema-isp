package spital.veterinar;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Client {

	private String nume;

	private String adresa;

	private List<Animal> animalList ;

	public Client(String nume, String adresa) {
		super();
		this.nume = nume;
		this.adresa = adresa;
		this.animalList = new ArrayList<Animal>();
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public List<Animal> getAnimalList() {
		return animalList;
	}

	public void setAnimalList(List<Animal> animalList) {
		this.animalList = animalList;
	}

	// nu are sens
	public String programareData(String data) {
		return null;
	}
	
	// nu are sens
	public Animal programareAnimal(Date data) {
		return null;
	}

	// nu are sens
	public List<ProceduriMedicale> programareProceduri() {
		return null;
	}
	
	public void addAnimal(Animal a) {
		animalList.add(a);
	}

}
