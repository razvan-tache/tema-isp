package spital.veterinar;
public class FisaInterventiiMedicale {

	private String numeVeterinar;

	private String diagnostic;

	public FisaInterventiiMedicale() {
		
	}
	
	public FisaInterventiiMedicale(String numeVeterinar, String diagnostic) {
		super();
		this.numeVeterinar = numeVeterinar;
		this.diagnostic = diagnostic;
	}

	public String getNumeVeterinar() {
		return numeVeterinar;
	}

	public void setNumeVeterinar(String numeVeterinar) {
		this.numeVeterinar = numeVeterinar;
	}

	public String getDiagnostic() {
		return diagnostic;
	}

	public void setDiagnostic(String diagnostic) {
		this.diagnostic = diagnostic;
	}

}
