package spital.veterinar;
import java.util.ArrayList;
import java.util.List;

public class Veterinar extends Angajat {

	private List<ProceduriMedicale> listaProceduri;

	public Veterinar(String nume, Float salariu) {
		super(nume, salariu);
		this.listaProceduri = new ArrayList<ProceduriMedicale>();
	}

	
	public List<ProceduriMedicale> getListaProceduri() {
		return listaProceduri;
	}

	public void setListaProceduri(List<ProceduriMedicale> listaProceduri) {
		this.listaProceduri = listaProceduri;
	}


	public String getPozitie() {
		return null;
	}

	public List<ProceduriMedicale> notificaCasier() {
		return null;
	}

	public List<ProceduriMedicale> addProceduri() {
		return null;
	}

}
