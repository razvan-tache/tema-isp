package spital.veterinar;
public class ProceduriMedicale extends Proceduri {

	private String tratament;

	private String observatiiMedicale;

	public ProceduriMedicale(String tratament, String observatiiMedicale) {
		super();
		this.tratament = tratament;
		this.observatiiMedicale = observatiiMedicale;
	}

	public String getTratament() {
		return tratament;
	}

	public void setTratament(String tratament) {
		this.tratament = tratament;
	}

	public String getObservatiiMedicale() {
		return observatiiMedicale;
	}

	public void setObservatiiMedicale(String observatiiMedicale) {
		this.observatiiMedicale = observatiiMedicale;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
