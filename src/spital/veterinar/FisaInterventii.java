package spital.veterinar;
public class FisaInterventii {

	private String numeAsistent;

	private String data;

	public FisaInterventii() {
		
	}
	
	public FisaInterventii(String numeAsistent, String data) {
		super();
		this.numeAsistent = numeAsistent;
		this.data = data;
	}

	public String getNumeAsistent() {
		return numeAsistent;
	}

	public void setNumeAsistent(String numeAsistent) {
		this.numeAsistent = numeAsistent;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
