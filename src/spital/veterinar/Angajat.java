package spital.veterinar;
public abstract class Angajat {

	private String nume;

	private Integer aniExperienta;

	private Float salariu;

	public Angajat(String nume, Float salariu) {
		super();
		this.aniExperienta = 0;
		this.nume = nume;
		this.salariu = salariu;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public Integer getAniExperienta() {
		return aniExperienta;
	}

	public void setAniExperienta(Integer aniExperienta) {
		this.aniExperienta = aniExperienta;
	}

	public Float getSalariu() {
		return salariu;
	}

	public void setSalariu(Float salariu) {
		this.salariu = salariu;
	}

	public abstract String getPozitie();
}
