package spital.veterinar;
public class Animal {

	private Integer id;

	private String specie;

	private String rasa;

	private FisaInterventii fisa;

	private FisaInterventiiMedicale fisaMedicala;

	public Animal(Integer id, String specie, String rasa) {
		super();
		this.id = id;
		this.specie = specie;
		this.rasa = rasa;
		this.fisa = new FisaInterventii();
		this.fisaMedicala = new FisaInterventiiMedicale();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSpecie() {
		return specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public String getRasa() {
		return rasa;
	}

	public void setRasa(String rasa) {
		this.rasa = rasa;
	}

	public FisaInterventii getFisa() {
		return fisa;
	}

	public void setFisa(FisaInterventii fisa) {
		this.fisa = fisa;
	}

	public FisaInterventiiMedicale getFisaMedicala() {
		return fisaMedicala;
	}

	public void setFisaMedicala(FisaInterventiiMedicale fisaMedicala) {
		this.fisaMedicala = fisaMedicala;
	}

}
