package spital.veterinar;

import java.util.Date;
import java.util.HashMap;

public class Proceduri {
	
	private String denumire;

	private String descriere;

	private Float pret;
	
	public Proceduri() {

	}

	public Proceduri(String denumire, String descriere, Float pret) {
		super();
		this.denumire = denumire;
		this.descriere = descriere;
		this.pret = pret;
	}

	public String getDenumire() {
		return denumire;
	}

	public void setDenumire(String denumire) {
		this.denumire = denumire;
	}

	public String getDescriere() {
		return descriere;
	}

	public void setDescriere(String descriere) {
		this.descriere = descriere;
	}

	public Float getPret() {
		return pret;
	}

	public void setPret(Float pret) {
		this.pret = pret;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Proceduri p = new Proceduri();
		p.denumire = this.denumire;
		p.descriere = this.descriere;
		p.pret = this.pret;
		
		return p;
	}
	
	
}
