package spital.veterinar.tests.registration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import spital.veterinar.Animal;
import spital.veterinar.Client;
import spital.veterinar.CoadaProgramare;
import spital.veterinar.Programare;
import spital.veterinar.Secretara;

public class TestRegistration {
	
	private Client client;
	private Secretara secretara;
	private Animal animal;
	
	@Before
	public void init() {
		client = new Client("Razvan Tache", "Splaiul independentei");
		secretara = new Secretara("Nume secretara", (float)2000);
		animal = new Animal(2, "C�ine", "Cocar");
		
		client.addAnimal(animal);
		
		ArrayList<String> s = new ArrayList<String>(0);
		
		s.add("tuns");
		s.add("proc 1");
		
		Date d = new Date(2015, 5, 20, 14, 30);
		secretara.doProgramare(client, animal, d, s);
	}
	
	@Test
	public void testProgramOnBusyDay() {
		ArrayList<String> s = new ArrayList<String>(0);
		
		s.add("tuns");
		s.add("proc 1");
		
		Date d = new Date(2015, 5, 20, 14, 30);

		Programare prog = secretara.doProgramare(client, animal, d, s);
		
		Assert.assertNull(prog);
	}
	
	@Test
	public void testProgram() {
		ArrayList<String> s = new ArrayList<String>(0);
		
		s.add("tuns");
		s.add("proc 1");
		
		Date d = new Date(2015, 5, 20, 15, 30);

		Programare prog = secretara.doProgramare(client, animal, d, s);
		
		Assert.assertTrue(CoadaProgramare.getInstance().getProgramari().contains(prog));
	}
	
	@Test
	public void testWorkflow() {
		ArrayList<String> s = new ArrayList<String>(0);
		
		s.add("tuns");
		s.add("proc 1");
		
		Date d = new Date(2015, 5, 20, 14, 30);

		Programare prog = secretara.doProgramare(client, animal, d, s);
		
		int hour = 14;
		while (prog == null) {
			hour ++;
			d = new Date(2015, 5, 20, hour, 30);

			prog = secretara.doProgramare(client, animal, d, s);
		}
		
		Assert.assertTrue(CoadaProgramare.getInstance().getProgramari().contains(prog));
	}

}
