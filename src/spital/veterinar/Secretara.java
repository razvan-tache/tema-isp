package spital.veterinar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Secretara extends Angajat {

	public Secretara(String nume, Float salariu) {
		super(nume, salariu);
	}

	public String getPozitie() {
		return "Secretara";
	}

	private void trimiteProcedura(Programare p) {
		CoadaProgramare.getInstance().addProgramare(p);
	}

	public Programare doProgramare(Client c, Animal a, Date d, List<String> proceduriCerute) {
		List<Programare> progamari = CoadaProgramare.getInstance().getProgramari();
		for (Programare p : progamari) {
			if (d.equals(p.getData())) {
				return null;
			}
		}
		
		HashMap<String, Proceduri> proceduriDisponibile = CoadaProgramare.lista_proceduri;
		List<Proceduri> newProcList = new ArrayList<Proceduri>();
		
		for (String numeProc : proceduriCerute) {
			Proceduri procedura = null;
			try {
				procedura = (Proceduri) proceduriDisponibile.get(numeProc).clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			newProcList.add(procedura);
		}
		
		Programare programare = new Programare(d, c, a);
		
		trimiteProcedura(programare);
		
		return programare;
	}

}
