package spital.veterinar;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Programare {

	private Date data;

	private Client client;

	private Animal animal;

	private List<Proceduri> proceduriList;

	public Programare(Date data, Client client, Animal animal) {
		super();
		this.data = data;
		this.client = client;
		this.animal = animal;
		this.proceduriList = new ArrayList<Proceduri>();
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Animal getAnimal() {
		return animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public List<Proceduri> getProceduriList() {
		return proceduriList;
	}

	public void setProceduriList(List<Proceduri> proceduriList) {
		this.proceduriList = proceduriList;
	}

}
