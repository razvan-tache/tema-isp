package spital.veterinar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CoadaProgramare {
	public static HashMap<String, Proceduri> lista_proceduri = getProceduriList();
	
	private static HashMap<String, Proceduri> getProceduriList() {
		HashMap<String, Proceduri> procs =  new HashMap<String, Proceduri>();
		procs.put("tuns", new Proceduri("tuns", "descriere tuns", (float) 50));
		procs.put("proc 1", new Proceduri("proc 1", "descriere proc 1", (float) 50));
		procs.put("proc 2", new Proceduri("proc 2", "descriere proc 2", (float) 50));
		procs.put("proc 3", new Proceduri("proc 3", "descriere proc 3", (float) 50));
		procs.put("proc 4", new Proceduri("proc 4", "descriere proc 4", (float) 50));
		procs.put("proc 5", new Proceduri("proc 5", "descriere proc 5", (float) 50));
		procs.put("proc 6", new Proceduri("proc 6", "descriere proc 6", (float) 50));
		procs.put("proc 7", new Proceduri("proc 7", "descriere proc 7", (float) 50));

		return procs;
	}
	
	static private CoadaProgramare instance = null;
	private List<Programare> coadaProgramari;
	
	private CoadaProgramare() {
		coadaProgramari = new ArrayList<Programare>();
	}
	
	public static CoadaProgramare getInstance() {
		if (instance == null) {
			instance = new CoadaProgramare();
		}
		
		return instance;
	}
	
	public void addProgramari(List<Programare> p) {
		coadaProgramari.addAll(p);
	}
	
	public void addProgramare(Programare p) {
		coadaProgramari.add(p);
	}
	
	public List<Programare> getProgramari() {
		return coadaProgramari;
	}
	
}
